# Widgets IDs, aka wid
str_the_wid__url_as_location: str = "dp-url__as_location"
str_the_wid__session_id_as_div: str = "dp-session-id__as_div"
str_the_wid__content_as_main: str = "dp-content__as_main"
str_the_wid__header_as_navbar: str = "dp-header__as_navbar"
str_the_wid__footer_as_footer: str = "dp-footer__as_footer"

# Widgets for debug
str_the_wid__debug_as_div: str = "dp-content__debug__as_div"
str_the_wid__debug_as_samp: str = "dp-content__debug__as_samp"
str_the_wid__debug_as_interval: str = "dp-content__debug__as_interval"

# #####################################################################################################################
# #####################################################################################################################
# Widgets for exploring
# #####################################################################################################################
# #####################################################################################################################

# the drop down to select a key
str_the_wid__explore__key__as_drop_down: str = "dp-content__explore__key__as_drop_down"

# the div containing the dash widget to graph the selected key
str_the_wid__explore__graph__as_div: str = "dp-content__explore__graph__as_div"

# the dash graph widget
str_the_wid__explore__graph__as_graph: str = "dp-content__explore__graph__as_graph"

# the div containing the table displaying selected key's details
str_the_wid__explore__table__as_div: str = "dp-content__explore__table__as_div"

str_the_wid__explore__table__prefix_to_access_columns: str = "dp-content__explore__table__"
str_the_wid__explore__table__suffix_to_access_columns: str = "__as_td"

str_the_wid__explore__table__key__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "key__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__measure_count__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "measure_count__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__date_first__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "date_first__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__date_late__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "date_last__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__measure_min__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "measure_min__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__measure_max__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "measure_max__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__measure_first__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "measure_first__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__measure_last__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "measure_last__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
str_the_wid__explore__table__predictability_last__as_td: str =\
    str_the_wid__explore__table__prefix_to_access_columns + "predictability_last__as_td" +\
    str_the_wid__explore__table__suffix_to_access_columns
