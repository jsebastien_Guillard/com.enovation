# TODO

* excel-dashboarder: we can use one single instance of ExcelDashboarder to produce several excel files:
  * Constructor should accept the configuration
  * Function "excelize" should only accept data to print in the excel
  * Objective: instantiating one singe dashboarder, that could be used to produce similar excel with different data

---

# A python tool box

Our python tool-box is supposed to help professionals in their day-to-day work.

Everything started when reaching blocking limitations with MS Excel, forcing us to search for alternatives. We learned Python, and started to implement reusable snippets.

As of 2021, we have a first workable snippet (yet perfectible), and we target to progressively (even-though 
slowly) enrich this tool-box, focusing on "IT project management" related stuff (as a start?).

In case you have feedbacks, ideas or suggestions, please let us know!

---

# Table of contents

* The project management toolbox
  * Predictability
    * Date
* The technicalities
  * PyPi versions
  * Install
  * Use
  * Contribute

---

# The project management toolbox

## Predictability

By definition, a good project manager is predictable, right?

But what do we mean by “being predictable”?
Let’s say that “Predictabilty” is about accurately predicting early enough the project outcomes, to enable timely corrective actions that increase the likelihood of achieving targets and reducing outcome variance.

Ok, fair enough. But how do we measure a project manager “predictable-ness”?

### Date predictability

The module `com.enovation.toolbox.predictability.dp_date_predictability` exposes several commands to deal with date predictability:

|   Command    | Description                                                                                                    |
|:------------:|:---------------------------------------------------------------------------------------------------------------|
| `dp_compute` | To compute the predictability for historical prediction of a date (eg. go live date, deal closure date)        |
| `dp_persist` | To persist into an excel file the outputs from the command `dp_compute`                                        |
|  `dp_load`   | To load the outputs from the command `dp_compute` that were persisted into an excel file                       |
|  `dp_graph`  | To visualize the outputs from the command `dp_compute` into a graph powered by [dash](https://dash.plotly.com) |
|  `dp_demo`   | To demonstrate some of the above command                                                                       |

---

# The technicalities

## PyPi Versions

* 0.0.5: date predictability - workable version
* 0.0.15: with a revamped readme.md, and 'demo' command, before sharing with "early adopters"
* 0.0.20: dp-demo fixed
* 0.0.21: click commands to handle json, excel dashboard with vba, dash with bubbles
* 0.0.22: enriched wheel including assets and json files
* 0.0.24: excel dashboard with "worksheet.set_column" and "worksheet.freeze_panes"
* 0.0.25: correction in a json schema for excel dashboarder: "merged" key word within widget_table_zoom.json
* 0.0.26: constant in ExcelDashboarder for "worksheet.set_column"
* 0.0.27: constants in ExcelDashboarder for "worksheet.set_column", updated dependencies, and several FutureWarnings fixed...
* 0.0.28: constants in ExcelDashboarder for "worksheet.set_column"
* 0.0.29: excel dashboard: parameterized workbook.options (to print NaN in excel)
* 0.0.32: excel dashboard: parameterized worksheet.set_row (to collapse/ hide rows...)
* 0.0.33: excel dashboard: worksheet.outline_settings() to group "above" and on the "left"
* 0.0.34: excel dashboard: minor adjustements
  * Correct number of lines in the table, without or without total row: depending on whether there is a total row, we adjust table range (before, there was an extra empty line when "no total row")
  * Correct conditional formatting range, which used to ignore the latest line in the table
  * Excel_dashboard > test_widget_table: in a table, add a "formula column" that references other columns... "formula": "=[[#This Row],[jsg formula col 2]]+5000"
* 0.0.35: excel dashboard: correction to handle pandas.NaTType values in table widget.
* 0.0.36: introduction of module "data handler"
* 0.0.37: Enhancement in data handler for datetime64:
  * Modifier 'type_columns': mapping 'date' to 'object' and 'datetime' to 'datetime64[ns]'
  * Modifier 'audit_column__stamped': transcode 's_column_to_stamp' to 'date' if 'datetime[64]' (to avoid mysterious exception...)
* 0.0.38: Adjustments in data handler
  * Modifier 'add_eval' > 'enov_lambda': added comment to illustrate, and 'axis' to process columns
  * Modifier 'add_eval': the calling dataframe can now be called from the expression. Comment added to illustrate
* 0.0.39: df-load-xls can now select the sheet to load
* 0.0.40: new Excel functions, adaptation to latest version for pandas.dataframe and to_datetime function which becomes stricter, packaging setup.cfg to project.toml

## Dependencies

When installing `com-enovation`, the following packages will be deployed automatically by `pip`:
* `pandas`: to handle dataframes, series, etc
* `click`: to handle command line
* `enlighten`: to display a progress bar for lengthy steps
* `openpyxl`: to handle `xlsx` files
* `xlwt`: that is a dependency for `pandas.io.excel`
* `xlrd`: yet another dependency for `pandas.io.excel`
* `dash`: to graph
* `scipy`: to compute date predictability without resampling measures, using `special.psi` function
* `xlsxwriter`: to produce excel spreadsheet. Used in `excel_dashboard`
* `jsonschema`: to check json parameters. Used in `excel_dashboard`
* `pywin32`: to access Windows API from Python, such as opening Excel, refreshing data and saving. Used in `helper.excel`. Note: only working on Windows... Cannot be installed on MAC or Linux.  

## Install

* Check `Python 3` is installed on your machine
* Check `pip` is installed on your machine
* Install the `com-enovation` tool-box: `python3 -m pip install com-enovation`
  * You can test by launching a python interpreter: `python3`
  * And load the package: `>>> import com.enovation`
* Ensure the deployed script is added to your `PATH`
* Upgrade the `com-enovation` tool-box: `pip install --upgrade com-enovation`

## Use

* You can get help by executing `enov -- help` in a terminal
* You have commands that you can run like `enov load-csv --help`
* Commands have:
  * Parameters that you can provide like `enov load-csv ./the-csv-file-to-load.csv`
  * Options that you can provide like `enov load-csv ./the-csv-file-to-load.csv -c the-first-column-label-to-load -c the-second-one`
* You can call for more logs by calling `enov --verbose load-csv ./the-csv-file-to-load.csv -c the-first-column-label-to-load -c the-second-one`

## Contribute

### Generate and publish the distribution

* from root directory
* build the distribution files and directories: `python3 -m build`
  * Directories `build` and `dist` should be generated
  * In case you face an error `No module named build`, you need first to run `pip install build`
* publish to `pypi`: `python3 -m twine upload --repository pypi dist/*`
  * In case you face an error `No module named twine`, you need first to run `pip install twine`
  * Package viewable at [pypi](https://pypi.org/project/com-enovation)

* Commands to execute from the root directory `com.enovation`

### Pycharm configuration

* In the left pan:
  * Directory `src`: mark as `Sources Root`
  * Directory `tests`: DO NOT mark as anything...

* Unit test configuration, from menu `Run > Edit Configurations...`
  * `Configuration > Target > Script path: /Users/jsg/PycharmProjects/com.enovation/tests`
  * `Configuration > Working directory: /Users/jsg/PycharmProjects/com.enovation/`
  * `Configuration > Add content roots to PYTHONPATH: checked`
  * `Configuration > Add source roots to PYTHONPATH: checked`

### Python stuff

* Check we have latest versions:
  * pip: `python3 -m pip install --upgrade pip`
  * build to generate the distribution: `python3 -m pip install --upgrade build`
  * twine to publish to pypi: `python3 -m pip install --upgrade twine`

* Update packages using pip
  * Check all packages are fine: `pip check`
  * List all packages outdated: `pip list --outdated`
  * Update all packages outdated: `pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U`
* A simple example package. You can use [Github-flavored Markdown](https://guides.github.com/features/mastering-markdown/) to write your content.

* To debug a running Click application:

### Git stuff

* To resynchronize local environment from remote "master" git
  * In the Local > Master
  * Fetch: we get the latest history from the remote
  * Update: we load the latest files locally (and the full tags are all on the head revision)